const { authJwt } = require("../middlewares");
const controller = require("../controllers/user.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/test/all", controller.allAccess);

  app.get(
    "/api/test/staff",
    [authJwt.verifyToken],
    controller.staffBoard
  );

  app.get(
    "/disposisi/valid",
    [authJwt.verifyToken],
    controller.disposisiAll
  );

  // app.get(
  //   "/api/test/kasi",
  //   [authJwt.verifyToken, authJwt.isModerator],
  //   controller.kasiBoard
  // );

  // app.get(
  //   "/api/test/pimpinan",
  //   [authJwt.verifyToken, authJwt.isAdmin],
  //   controller.pimpinanBoard
  // );

  // app.get(
  //   "/api/test/petugas_agenda",
  //   [authJwt.verifyToken, authJwt.isAdmin],
  //   controller.agendaBoard
  // );
};
const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");

verifyToken = (req, res, next) => {
  let token = req.headers["x-access-token"];

  if (!token) {
    return res.status(403).send({
      status : "0",
      statusCode : 403,
      message: "No token provided!"
    });
  }

  jwt.verify(token.replace("token=", ""), config.secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({
        status : "0",
        statusCode : 401,
        message: "Unauthorized!"
      });
    }
    req.userId = decoded.id_master
    next();
  });
};

const authJwt = {
  verifyToken: verifyToken
};

module.exports = authJwt;
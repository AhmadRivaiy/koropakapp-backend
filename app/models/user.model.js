const config = require("../config/db.config.js");
const {Op,Model, QueryTypes,DataTypes, Sequelize} = require('sequelize');
const sequelize = new Sequelize(
  config.DB,
  config.USER,
  config.PASSWORD,
  {
    host: config.HOST,
    dialect: config.dialect,
    operatorsAliases: 0,

    pool: {
      max: config.pool.max,
      min: config.pool.min,
      acquire: config.pool.acquire,
      idle: config.pool.idle
    }
  }
);

const User = sequelize.define("tbl_users", {
  id_master: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  email: {
    type: Sequelize.STRING
  },
  password: {
    type: Sequelize.STRING
  }
}, {
  freezeTableName: true,
  timestamps: false
});

const Disposisi = sequelize.define("disposisi", {
  id_disposisi: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  isi_disposisi: {
    type: Sequelize.STRING
  },
  sifat: {
    type: Sequelize.STRING
  },
  id_master: {
    type: Sequelize.INTEGER
  }
}, {
  freezeTableName: true,
  timestamps: false
});

class UserModel {
  getUser = (username) => {
      return User.findOne({
          where: {
            email: username
          }
        })
        
  }

  getDataDisposisi = (id_master) => {
    return Disposisi.findAll({
      where: {
          id_master: id_master
        }
      })
  }
}
module.exports = UserModel;
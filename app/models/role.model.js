module.exports = (sequelize, Sequelize) => {
    const Role = sequelize.define("ref.hak_akses", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
      name: {
        type: Sequelize.STRING
      }
    });
  
    return Role;
  };

const config = require("../config/auth.config");
const UserModel = require("../models/user.model");

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

exports.signin = (req, res) => {
  new UserModel().getUser(req.body.username)
    .then(user => {
      // if (!user) {
      //   return res.status(404).send({ message: "User Not found." });
      // }

      var token = jwt.sign({ id_master: user.id_master }, config.secret, {
        expiresIn: 86400 // 24 hours
      });

      res.status(200).send({
        data : {
          status : '1',
          message : 'Login Sukses!',
          payload : {
            id: user.id_master,
            username: user.username,
            email: user.email,
            password: user.password,
            accessToken: token
          }
        }
      });
    }).catch(err => {
      var details = {
        parent: err.parent,
        name: err.name,
        message: err.message
      }

      res.status(404).send({
        data : {
          status : '0',
          message : 'User Not Found',
          date: new Date(),
          details: details
        }
      });
    });
};
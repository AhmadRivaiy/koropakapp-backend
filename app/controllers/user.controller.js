const UserModel = require("../models/user.model");

exports.allAccess = (req, res) => {
    res.status(200).send("Public Content.");
};
  
exports.staffBoard = (req, res) => {
    res.status(200).send("Staff Content.");
};

exports.disposisiAll = (req, res) => {
    new UserModel().getDataDisposisi(req.userId)
    .then(x => {
      res.status(200).send({
        statusCode : 200,
        list_disposisi : x
      });
    }).catch(err => {
      var details = {
        parent: err.parent,
        name: err.name,
        message: err.message
      }

      res.status(404).send({
        data : {
          status : '0',
          statusCode : 404,
          message : 'Disposisi Kosong!',
          date: new Date(),
          details: details
        }
      });
    });
};
exports.kasiBoard = (req, res) => {
    res.status(200).send("Kasi Content.");
};
  
exports.pimpinanBoard = (req, res) => {
    res.status(200).send("Pimpinan Content.");
};

exports.agendaBoard = (req, res) => {
    res.status(200).send("Petugas Agenda Content.");
};